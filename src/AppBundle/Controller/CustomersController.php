<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Exception\BusinessException;

class CustomersController extends Controller
{
    /**
     * @Route("/customers/")
     * @Method("GET")
     */
    public function getAction()
    {
      try {
        return new JsonResponse(
          $this->get('customer_repository')->getAll()
        );
      } catch(\Exception $e) {
        return $this->returnUnknownException();
      }
    }

    /**
     * @Route("/customers/")
     * @Method("POST")
     */
    public function postAction(Request $request)
    {
      $customers = json_decode($request->getContent());

      try {
        $this->get('customer_repository')->insertMany($customers);
      } catch(BusinessException $e) {
        return (new JsonResponse(['status' => $e->getMessage()]))->setStatusCode(412);
      } catch(\Exception $e) {
        return $this->returnUnknownException();
      }

      return (new JsonResponse(['status' => 'Customers successfully created']))
        ->setStatusCode(201);
    }

    /**
     * @Route("/customers/")
     * @Method("DELETE")
     */
    public function deleteAction()
    {
      try {
        $this->get('customer_repository')->deleteAll();
      } catch(\Exception $e) {
        return $this->returnUnknownException();
      }

      return (new JsonResponse())->setStatusCode(204);
    }

    private function returnUnknownException()
    {
      return (new JsonResponse(['status' => 'An error ocurred, try again later']))
        ->setStatusCode(500);
    }
}
