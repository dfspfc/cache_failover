<?php

namespace AppBundle\Exception;

class CustomerRequiredException extends BusinessException
{
  public function __construct() {
      parent::__construct('You must pass at least one customer');
  }
}
