<?php

namespace AppBundle\Service;

class CacheService
{
    private $cache;

    public function __construct($cache)
    {
      $this->cache = $cache;
    }

    public function getCache()
    {
      return $this->cache;
    }
}
