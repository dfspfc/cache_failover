<?php

namespace AppBundle\Service;

class CustomerCache implements
  PartOfGetAllChainInterface,
  GetsAllInterface
{
    use PartOfGetAllChainTemplateTrait;

    const CUSTOMERS_KEY = 'customers';

    private $cache;
    private $logger;

    public function __construct($cache, $logger)
    {
        $this->cache = $cache->getCache();
        $this->logger = $logger;
    }

    public function getAll()
    {
      $allCustomers = '';

      try {
        if($this->cache) {
          $allCustomers = $this->cache->get(self::CUSTOMERS_KEY);
        }
      } catch (\Exception $e) {
        $this->logInCaseOfRedisMalFunction($e);
      }

      if(!$allCustomers) {
        return $this->nextPartOfGetAllChain->getAll();
      }

      return $allCustomers;
    }

    public function insertMany(array $newCustomers)
    {
      $allCustomersToSetOnCache = $newCustomers;

      try{
        if ($this->isThereAnyCustomerOnCache()) {
          $allCustomersToSetOnCache = $this->mergeNewCustomersWithCustomersOnCache($newCustomers);
        }

        $this->cache->set(self::CUSTOMERS_KEY, json_encode($allCustomersToSetOnCache));
      } catch (\Exception $e) {
        $this->logInCaseOfRedisMalFunction($e);
      }
    }

    public function deleteAll()
    {
      try{
        return $this->cache->del(self::CUSTOMERS_KEY);
      } catch (\Exception $e) {
        $this->logInCaseOfRedisMalFunction($e);
      }
    }

    private function isThereAnyCustomerOnCache()
    {
      return $this->cache->exists(self::CUSTOMERS_KEY);
    }

    private function mergeNewCustomersWithCustomersOnCache($newCustomers)
    {
      $customersOnCache = json_decode($this->cache->get(self::CUSTOMERS_KEY));

      return array_merge($newCustomers, $customersOnCache);
    }

    private function logInCaseOfRedisMalFunction(\Exception $e)
    {
      $this->logger->error($e->getMessage());
    }
}
