<?php

namespace AppBundle\Service;

class CustomerModel implements
  PartOfGetAllChainInterface,
  GetsAllInterface
{
    use PartOfGetAllChainTemplateTrait;

    private $database;

    public function __construct($database)
    {
        $this->database = $database->getDatabase();
    }

    public function getAll()
    {
      return json_encode(
        iterator_to_array($this->database->customers->find())
      );
    }

    public function insertMany(array $customers)
    {
      foreach ($customers as $customer) {
        $this->database->customers->insert($customer);
      }
    }

    public function deleteAll()
    {
      return $this->database->customers->drop();
    }
}
