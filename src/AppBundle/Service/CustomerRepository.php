<?php

namespace AppBundle\Service;

use AppBundle\Exception\CustomerRequiredException;

class CustomerRepository implements GetsAllInterface
{
  private $customerModel;
  private $customerCache;

  public function __construct($customerModel, $customerCache) {
    $this->customerModel = $customerModel;
    $this->customerCache = $customerCache;
  }

  public function getAll()
  {
    $this->customerCache
      ->setNextPartOfGetAllChain($this->customerModel);

    return $this->customerCache->getAll();
  }

  public function insertMany(array $customers)
  {
    if (!$this->isThereAtLeastOneCustomer($customers)) {
      throw new CustomerRequiredException();
    }

    $this->customerModel->insertMany($customers);
    $this->customerCache->insertMany($customers);
  }

  public function deleteAll()
  {
    $this->customerModel->deleteAll();
    $this->customerCache->deleteAll();
  }

  private function isThereAtLeastOneCustomer($customers)
  {
    return count($customers) > 0;
  }
}
