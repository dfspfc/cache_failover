<?php

namespace AppBundle\Service;

use \MongoClient;
use \MongoDB;

class DatabaseService
{
    protected $database;

    public function __construct($host, $port, $database)
    {
        $mongoClient = new MongoClient("mongodb://$host:$port/");
        $this->database = $mongoClient->selectDB($database);
    }

    public function getDatabase()
    {
        return $this->database;
    }
}
