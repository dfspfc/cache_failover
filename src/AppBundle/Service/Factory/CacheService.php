<?php

namespace AppBundle\Service\Factory;

use AppBundle\Service\CacheService as CacheServiceService;
use Predis\Client as PredisClient;

class CacheService
{
  private $host;
  private $port;
  private $prefix;

  public function __construct($host, $port, $prefix)
  {
    $this->host = $host;
    $this->port = $port;
    $this->prefix = $prefix;
  }

  public function create()
  {
    return new CacheServiceService(
      new PredisClient(["host" => $this->host,"port" => $this->port])
    );
  }
}
