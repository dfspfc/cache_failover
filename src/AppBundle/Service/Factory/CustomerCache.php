<?php

namespace AppBundle\Service\Factory;

use AppBundle\Service\CustomerCache as CustomerCacheService;

class CustomerCache
{

  public function __construct($contatiner)
  {
    $this->contatiner = $contatiner;
  }

  public function create()
  {
    return new CustomerCacheService(
      $this->contatiner->get('cache_service'),
      $this->contatiner->get('logger')
    );
  }
}
