<?php

namespace AppBundle\Service\Factory;

use AppBundle\Service\CustomerModel as CustomerModelService;

class CustomerModel
{

  public function __construct($contatiner)
  {
    $this->contatiner = $contatiner;
  }

  public function create()
  {
    return new CustomerModelService($this->contatiner->get('database_service'));
  }
}
