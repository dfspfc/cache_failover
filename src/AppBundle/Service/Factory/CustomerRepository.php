<?php

namespace AppBundle\Service\Factory;

use AppBundle\Service\CustomerRepository as CustomerRepositoryService;

class CustomerRepository
{

  public function __construct($contatiner)
  {
    $this->contatiner = $contatiner;
  }

  public function create()
  {
    //var_dump($contatiner); die;
    return new CustomerRepositoryService(
      $this->contatiner->get('customer_model'),
      $this->contatiner->get('customer_cache')
    );
  }
}
