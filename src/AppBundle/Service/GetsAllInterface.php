<?php

namespace AppBundle\Service;

interface GetsAllInterface
{
  public function getAll();
}
