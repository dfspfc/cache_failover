<?php

namespace AppBundle\Service;

interface PartOfGetAllChainInterface
{
  public function setNextPartOfGetAllChain(
    PartOfGetAllChainInterface $nextPartOfGetAllChain
  );
}
