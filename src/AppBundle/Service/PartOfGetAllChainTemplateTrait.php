<?php

namespace AppBundle\Service;

trait PartOfGetAllChainTemplateTrait
{
  private $nextPartOfGetAllChain;

  public function setNextPartOfGetAllChain(
    PartOfGetAllChainInterface $nextPartOfGetAllChain
  ) {
    $this->nextPartOfGetAllChain = $nextPartOfGetAllChain;
  }
}
