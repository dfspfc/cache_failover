<?php

namespace AppBundle\Tests\Service;

use AppBundle\Tests\MyTestCase;

class CustomerRepositoryTest extends MyTestCase
{
  private $customerRepository;

  public function setUp()
  {
    $this->client = self::createClient();
    $this->customerRepository = $this->client->getContainer()
      ->get('customer_repository');
  }

  public function testClassExists()
  {
    $this->assertInstanceOf(
      'AppBundle\Service\CustomerRepository',
      $this->customerRepository
    );
  }

  public function testGetAllShouldHitDataBaseIfRedisIsDown()
  {
    $returnFromModel = '[{"name":"leandro", "age":26}, {"name":"marcio", "age":30}]';

    $mockCacheService  = $this->getMockCacheService(null);
    $mockCustomerModel = $this->getMockCustomerModel($returnFromModel);

    $this->setKernelModifier(function () use ($mockCacheService, $mockCustomerModel) {
      static::$kernel->getContainer()->set('cache_service', $mockCacheService);
      static::$kernel->getContainer()->set('customer_model', $mockCustomerModel);
    });

    $client = static::createClient();

    $this->assertEquals(
      $returnFromModel,
      $client->getContainer()->get('customer_repository')->getAll()
    );
  }

  public function testGetAllShouldHitDataBaseIfRedisReturnsEmpty()
  {
    $returnFromDataBase = '[{"name":"leandro", "age":26}, {"name":"marcio", "age":30}]';

    $mockCustomerModel = $this->getMockCustomerModel($returnFromDataBase);
    $cacheService = $this->getMockCacheServiceMockingPredis('');

    $this->setKernelModifier(function () use ($cacheService, $mockCustomerModel) {
      static::$kernel->getContainer()->set('cache_service', $cacheService);
      static::$kernel->getContainer()->set('customer_model', $mockCustomerModel);
    });

    $client = static::createClient();

    $this->assertEquals(
      $returnFromDataBase,
      $client->getContainer()->get('customer_repository')->getAll()
    );
  }

  /**
   * @expectedException     \AppBundle\Exception\CustomerRequiredException
   * @expectedExceptionMessage You must pass at least one customer
   */
  public function testInsertCustomersWithInvalidParameterShouldThrowAnException()
  {
      $this->customerRepository->insertMany([]);
  }

  private function getMockCacheService($getCacheReturn)
  {
    $cacheService = $this->getMockBuilder('AppBundle\Service\CacheService')
      ->disableOriginalConstructor()
      ->getMock();

    $cacheService->method('getCache')
      ->willReturn($getCacheReturn);

    return $cacheService;
  }

  private function getMockCacheServiceMockingPredis($getReturn)
  {
    $cacheService = $this->getMockBuilder('AppBundle\Service\CacheService')
      ->setMethods(['__construct'])
      ->setConstructorArgs([$this->getPredisMock($getReturn)])
      ->getMock();

    return $cacheService;
  }

  private function getMockCustomerCache($getAllReturn)
  {
    $customerCache = $this->getMockBuilder('AppBundle\Service\CustomerCache')
      ->disableOriginalConstructor()
      ->getMock();

    $customerCache->method('getAll')
      ->willReturn($getAllReturn);

    return $customerCache;
  }

  private function getMockCustomerModel($getAllReturn)
  {
    $customerModel = $this->getMockBuilder('AppBundle\Service\CustomerModel')
      ->disableOriginalConstructor()
      ->getMock();

    $customerModel->method('getAll')
      ->willReturn($getAllReturn);

    return $customerModel;
  }

  private function getPredisMock($getReturn)
  {
    $predis = $this->getMockBuilder('\Predis\Client')
      ->disableOriginalConstructor()
      ->getMock();

    $predis->method('get')
      ->willReturn($getReturn);

    return $predis;
  }
}
