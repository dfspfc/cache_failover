Feature: Delete Customers

    Scenario: Delete customers

        Given I make a "DELETE" request for "http://127.0.0.1:8000/customers/"
        Then the status code response should be "204"
