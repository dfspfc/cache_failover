Feature: Get Customers
    @run
    Scenario: Get customers

        Given I make a "GET" request for "http://127.0.0.1:8000/customers/"
        Then the status code response should be "200"
